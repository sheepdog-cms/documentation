Welcome to Sheepdog CMS's docs
========================================

These developer pages document how to use Sheepdog's API to empower
your applications and create awesome extensions.

All of [our documentation is on Gitlab](https://gitlab.com/sheepdog-cms/documentation) and we would love corrections and improvements!

.. toctree::
   :maxdepth: 2
   :caption: Contents:
Stacks
========================================

Stacks is the way we handle extensions. If something ain't right, the issue lies probably on the manifest (*IT'S ALL YOUR FAULT, MANIFEST!!!**) level.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Manifest
========================================

**Extension Manifest File** (stacks.json file) is a must-have for every extension.
How can Sheepdog know that the "*Cat Gifs Instead Articles*" extension wants to be
a content parser, and not a wild upload interrupter?!

A typical Stacks-compatible manifest consists of **Autoload** strings array (so that's
how these quantum hamsters load that ugly, disgusting CatGifsComScrapper class!)
and a **Layers** map/dictionary.

A **layer** is an execution point. You don't want that mean, nasty toaster to participate in database
connection establishment, right?!

You hook up some class with a layer by... umm... nevermind, just think of that layer address as of
a key and use the class address as a value. Sorry, no multiple assignments! If you need them,
either make one class that calls the others or use the **Stacks::RegisterLayerCall** function.

    void RegisterLayerCall (
	    string $Layer,
		string $CallAddress
	)

Some layers get extra parameters and/or expect some response.

List of Layers
========================================

|----------|-------------------------------------------|------------|--------------|
| Category | Layer                                     | Parameters | Return Value |
|----------|-------------------------------------------|------------|--------------|
| Admin    | Sheepdog\\AdminAPI\\GenerateSettings      | -          | array        |
| Routing  | Sheepdog\\Routes\\AfterDefaults           | -          | -            |
|----------------------------------------------------------------------------------|